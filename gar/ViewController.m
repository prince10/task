//
//  ViewController.m
//  gar
//
//  Created by Prince on 09/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imageView1;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UIImageView *imageView2;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UITextField *textField1;
@property (strong, nonatomic) IBOutlet UITextField *textField2;
@property (strong, nonatomic) IBOutlet UITextField *textField3;
@property (strong, nonatomic) IBOutlet UITextField *textField4;
@property (strong, nonatomic) IBOutlet UIButton *buttonDone;
@property (strong, nonatomic) IBOutlet UIButton *changeButton;

@end

@implementation ViewController
@synthesize imageView1;
@synthesize imageView2;
@synthesize buttonDone;
@synthesize textField1;
@synthesize textField2;
@synthesize textField3;
@synthesize textField4;

- (void)viewDidLoad {

    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(IBAction)donePressed:(id)sender{
    
    if ([self FirstName:textField1.text]==NO) {
        [self alert1];
    }
    else if ([self LastName:textField2.text]==NO){
        [self alert2 ];
    }
    else if ([self email:textField3.text]==YES){
        [self alert3 ];
    }
    else if ([self phoneno:textField4.text]==NO){
        [self alert4];
    }
        else if (textField1.text==nil){
            [self alert5];
    
        }
        else if (textField2.text.length==0){
            [self alert5];
        }
        else if ([textField3.text length]==0){
            [self alert5];
        }
        else if ([textField4.text length]==0){
            [self alert5];
       }

}
-(BOOL) FirstName:(NSString *)text
{
    NSCharacterSet *set=[[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location != NSNotFound)
    {
        NSLog(@"no special character");
        return NO;
        
    }
    else
    {
        NSLog(@"has special character");
        return YES;
    }
}


-(BOOL) LastName:(NSString *)text
{
    NSCharacterSet *set=[[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location != NSNotFound)
    {
        NSLog(@"no special character");
        return NO;
        
    }
    else
    {
        NSLog(@"has special character");
        return YES;
    }
}

- (BOOL)email:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
if ([emailTest evaluateWithObject:textField3.text]==YES)
    {
        return NO;
    }
    else
    {
        return YES;
    }



}
-(BOOL) phoneno:(NSString *)text
{
    NSCharacterSet *set=[[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location != NSNotFound)
    {
        NSLog(@"no special character");
        return NO;
        
    }
    else
    {
        NSLog(@"has special character");
        return YES;
    }
}





-(void) alert1
{
    
    UIAlertView * alert55 = [[UIAlertView alloc]initWithTitle:@" In first name " message:@"Only Charecter Are Allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert55 show];
}

    -(void) alert2
    {
        
        UIAlertView * alert66 = [[UIAlertView alloc]initWithTitle:@"in last name " message:@"Only Charecter Are Allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert66 show];
    }

        -(void) alert3
        {
            
            UIAlertView * alert77 = [[UIAlertView alloc]initWithTitle:@"in email " message:@"enter correct mail" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert77 show];
        }

            -(void) alert4
            {
                
                UIAlertView * alert88 = [[UIAlertView alloc]initWithTitle:@"phone no" message:@"enter correct phone no." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alert88 show];


            }


-(void) alert5
{
    
    UIAlertView * alert55 = [[UIAlertView alloc]initWithTitle:@"Alert " message:@"All field are madinatory" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert55 show];
    
}






- (IBAction)changePressed:(id)sender {
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
